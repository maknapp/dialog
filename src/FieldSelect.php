<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldSelect extends Field
{
    private  int $length = 1;
    private  string $default = "";
    public    array $options = [];

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['length'])) $this->length = (int) $attributes["length"];
        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"];
        // options
        foreach ($node->children() as $child){
            if($child->getName() === 'option'){
                if(!is_null($child->attributes()['label']) && !is_null($child->attributes()['value']))
                    $this->options[(string) $child->attributes()['value']] = (string) $child->attributes()['label'];
                else if(!is_null($child->attributes()['value'])) $this->options[] = $child->attributes()['value'];
            }
        }
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            $element['type'] = 'select';
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }
        $element['options'] = $this->options;

        return $element;
    }
}