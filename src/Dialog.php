<?php

namespace Maknapp\Dialog;

use ErrorException;
use SimpleXMLElement;

class Dialog
{
    public string $name = "";
    private SimpleXMLElement $format;
    private array $elements = [];
    public string $title = "";
    public string $action = "";
    public bool   $buttons = true;
    private ?string $html = null;

    public function __construct(string $path = "", string $name = "")
    {
        if(file_exists("$path.xml")){
            $file = "$path.xml";
            if (file_exists($file)) {
                $this->format = new SimpleXMLElement(file_get_contents($file));
                $attributes = $this->format->attributes();
                if(!is_null($attributes['name'])) $this->name = (string) $attributes["name"];
                if(!is_null($attributes['title'])) $this->title = (string) $attributes["title"];
                if(!is_null($attributes['action'])) $this->action = (string) $attributes["action"];

                $nodes = $this->format->children();
                foreach ($nodes as $node) {
                    $node_name = (string) $node->attributes()['name'];
                    try{
                        $this->elements[$node_name] = match ($node->getName()) {
                            "integer"   => new FieldInteger($this->name, $node),
                            "real"      => new FieldReal($this->name, $node),
                            "richtext"  => new FieldRichtext($this->name, $node),
                            "text"      => new FieldText($this->name, $node),
                            "password"  => new FieldPassword($this->name, $node),
                            "boolean"   => new FieldBoolean($this->name, $node),
                            "file"      => new FieldFile($this->name, $node),
                            "select"    => new FieldSelect($this->name, $node),
                            "timestamp" => new FieldTimestamp($this->name, $node),
                            default     => throw new ErrorException(get_class($this) . " Unknown datatype {$node->getName()}"),
                        };
                    } catch (ErrorException){
                        try {
                            $class = '\Maknapp\Dialog\\Field'.ucfirst($node->getName());
                            $this->elements[$node_name] = new $class($this->name, $node);
                        } catch (\Exception) {
                            throw new ErrorException(get_class($this) . " Unknown datatype {$node->getName()}");
                        }
                    }
                }
            }
        } elseif($name !== "") {
            $this->name = $name;
        }
    }

    // FORMULAR
    public function getJSON(array $values = []): string
    {

        $dialog = [
            "title" => $this->title,
            "action" => $this->action,
            "content" => []
        ];

        // HTML
        if(!is_null($this->html)){
            $dialog['content'][] = [
                "type" => "html",
                "value" => $this->html
            ];
        }

        // Form Fields
        if(sizeof($this->elements) > 0){
            $dialog_elements = [];
            foreach ($this->elements as $name => $element) {
                /** @var Field $element */
                if(array_key_exists($name, $values)){
                    $element->value = $values[$name];
                }
                $dialog_element = $element->getDialogField($values);
                if(isset($dialog_element[0])){
                    $dialog_elements = array_merge_recursive($dialog_elements, $dialog_element);
                }
                else $dialog_elements[] = $dialog_element;
            }
            $dialog['content'][] = [
                "type" => "form",
                "fields" => $dialog_elements
            ];
        }

        // Buttons
        if($this->buttons){
            $dialog['content'][] = [
                'type' => 'button',
                'value' => [
                    [
                        'type' => 'cancel',
                        'label' => 'cancel'
                    ],[
                        'type' => 'submit',
                        'label' => 'save'
                    ]
                ]
            ];
        }
        return json_encode($dialog);
    }

    public function getElement($name): Field{
        return $this->elements[$name];
    }

    public function appendElement(Field $element){
        $this->elements[$this->name.'.'.$element->name] = $element;
    }

    public function setHTML(string $html){
        $this->html = $html;
    }
}