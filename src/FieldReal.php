<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldReal extends Field
{
    private  int  $length = 1;
    private  int  $default = 0;
    private float $step = 0.0000001;
    private ?int  $min = null;
    private ?int  $max = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['length'])) $this->length = (int) $attributes["length"];
        if(!is_null($attributes['default'])) $this->default = (int) $attributes["default"];
        if(!is_null($attributes['min'])) $this->min = (int) $attributes["min"];
        if(!is_null($attributes['max'])) $this->max = (int) $attributes["max"];
        if(!is_null($attributes['step'])) $this->step = (float) $attributes["step"];
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element) && !isset($element[0])){
            $element['type'] = 'number';
            $element['step'] = $this->step;
            $element['min'] = is_null($this->min) ? (2**(8*$this->length)) / - 2 : $this->min;
            $element['max'] = is_null($this->max) ? (2**(8*$this->length)) / 2 - 1 : $this->max;
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }

        return $element;
    }
}