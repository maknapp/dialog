<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldRichtext extends Field
{
    private  string $default = "";

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"];
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            $element['type'] = 'richtext';
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }

        return $element;
    }
}