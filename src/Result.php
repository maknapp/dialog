<?php

namespace Maknapp\Dialog;

class Result
{
    /*
    CODE:
    0 - Failed
    1 - Ok
    */
    public int $code = 0;
    public string $message;
    public string $redirect;

    public function __construct()
    {
    }

    public function getJSON(): string
    {
        $json = [];
        if(isset($this->code)) $json["code"] = $this->code;
        if(isset($this->message)) $json["message"] = $this->message;
        if(isset($this->redirect)) $json["redirect"] = $this->redirect;
        return json_encode($json);
    }
}