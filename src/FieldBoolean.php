<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldBoolean extends Field
{
    private  bool $default = false;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"] !== "false";
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            $element['type'] = 'switch';
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }

        return $element;
    }
}