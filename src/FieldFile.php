<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldFile extends Field
{
    private ?string $mime = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['mime'])) $this->mime = (string) $attributes["mime"];
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            $element['type'] = 'file';
            $element['accept'] = !is_null($this->mime) ? $this->mime : '*';
        }

        return $element;
    }
}