<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldTimestamp extends Field
{
    private  string $default = "";
    private  string $format = "";

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"];
        if(!is_null($attributes['format'])) $this->format = (string) $attributes["format"];
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            switch ($this->format){
                case 'date':
                    $element['type'] = 'date';
                    break;
                case 'time':
                    $element['type'] = 'time';
                    break;
                default:
                    $element['type'] = 'datetime-local';
                    break;
            }
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }

        return $element;
    }
}