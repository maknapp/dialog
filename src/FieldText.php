<?php

namespace Maknapp\Dialog;

use SimpleXMLElement;

class FieldText extends Field
{
    private  int $length = 1;
    private  string $default = "";
    private ?bool $textWrap = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['length'])) $this->length = (int) $attributes["length"];
        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"];
        if(!is_null($attributes['text-wrap'])) $this->textWrap = (string) $attributes["text-wrap"] === 'true';
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        if(!array_key_exists('type', $element)) {
            $element['type'] = 'text';
            $element['value'] = !is_null($this->value) ? $this->value : $this->default;
        }
        if(!is_null($this->textWrap)) $element['textWrap'] = $this->textWrap;

        return $element;
    }
}