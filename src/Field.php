<?php

namespace Maknapp\Dialog;

use ErrorException;
use SimpleXMLElement;

class Field
{
    public     string $namespace;
    public     string $name = "";
    protected  bool   $null = true;
    public     bool   $visibility = true;
    public     bool   $disabled = false;
    public     bool   $readonly = false;
    public    ?string $type = null;
    protected ?string $label = null;
    protected ?string $hint = null;
    protected ?string $hintImage = null;
    protected ?string $placeholder = null;
    protected ?array  $requires = null;
    public     mixed  $value = null;
    private   ?bool   $keyEvents = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        $this->namespace = $namespace;
        $attributes = $node->attributes();

        if(!is_null($attributes['name'])) $this->name = (string) $attributes["name"];
        else throw new ErrorException('Table element need the attribute name');
        if(!is_null($attributes['null'])) $this->null = (bool) $attributes["null"];
        if(!is_null($attributes['visibility'])) $this->visibility = (string) $attributes["visibility"] !== "false";
        if(!is_null($attributes['label'])) $this->label = (string) $attributes["label"];
        if(!is_null($attributes['hint'])) $this->hint = (string) $attributes["hint"];
        if(!is_null($attributes['hint-image'])) $this->hintImage = (string) $attributes["hint-image"];
        if(!is_null($attributes['placeholder'])) $this->placeholder = (string) $attributes["placeholder"];
        if(!is_null($attributes['requires'])){
            $fun = function(string $values){
                $req = explode(":", $values);
                return [$this->namespace.'.'.$req[0], $req[1]];
            };
            $this->requires = array_map($fun, explode(";", (string) $attributes['requires']));
        }
        if(!is_null($attributes['keyevents'])) $this->keyEvents = (string) $attributes["keyevents"] === 'true';
    }

    public function getDialogField(array $values): array
    {
        $element = [
            'name' => $this->namespace.'.'.$this->name,
        ];
        if($this->disabled) $element['disabled'] = $this->disabled;
        if($this->readonly) $element['readonly'] = $this->readonly;
        if(!is_null($this->label)) $element['label'] = $this->label;
        if(!is_null($this->hint)) $element['hint'] = $this->hint;
        if(!is_null($this->hintImage)) $element['hintImage'] = $this->hintImage;
        if(!is_null($this->placeholder)) $element['placeholder'] = $this->placeholder;
        if(!is_null($this->requires)) $element['requires'] = $this->requires;
        if(!$this->visibility){
            $element['type'] = 'hidden';
            if(!is_null($this->value)) $element['value'] = $this->value;
        } else if($this->type === "select") {
            $element['type'] = 'select';
            if(!is_null($this->value)) $element['value'] = $this->value;
        }
        if(!is_null($this->keyEvents)) $element['keyevents'] = $this->keyEvents;

        return $element;
    }
}