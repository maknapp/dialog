var Maknapp;
(function (Maknapp) {
    const dialog_field = {
        'input': function (dialog, DOMtableTR, field) {
            let DOM_tableTR_name = document.createElement('th');
            DOMtableTR.appendChild(DOM_tableTR_name);
            if (field.hasOwnProperty('type') && field.type !== 'hidden') {
                DOM_tableTR_name.className = 'r';
                DOM_tableTR_name.innerHTML = field.label;
                if ((field.hasOwnProperty('hint') && typeof field.hint === 'string') || (field.hasOwnProperty('hintImage') && typeof field.hintImage === 'string')) {
                    let hintContent = '';
                    if (field.hasOwnProperty('hint')) {
                        hintContent += field.hint;
                    }
                    if (field.hasOwnProperty('hintImage')) {
                        hintContent += `<br><img src="${field.hintImage}" alt="hint" width="150" />`;
                    }
                    DOM_tableTR_name.innerHTML += `<div class="tooltip"><span class="material-icons clickable" style="font-size: 16px;margin-left: 8px;">help</span><div class="tooltip_text">${hintContent}</div></div>`;
                }
            }
            let DOM_tableTR_input = document.createElement('td');
            DOMtableTR.appendChild(DOM_tableTR_input);
            DOM_tableTR_input.className = 'c';
            let DOMmother = document.createElement('div');
            DOM_tableTR_input.appendChild(DOMmother);
            let DOMinput = document.createElement('input');
            if (field.type === "switch") {
                DOMinput.type = "checkbox";
                DOMinput.className = 'switch';
                DOMinput.id = field.name;
            }
            else if (field.type === 'text' && typeof field.textWrap !== 'undefined' && field.textWrap === true) {
                DOMinput = document.createElement('textarea');
                DOMinput.className = 'input';
                DOMinput.setAttribute('wrap', 'soft');
                DOMinput.addEventListener('keydown', function (event) {
                    if (event.key === 'Enter') {
                        event.preventDefault();
                        return false;
                    }
                });
            }
            else
                DOMinput.type = field.type;
            DOMinput.name = field.name;
            if (typeof field.placeholder !== 'undefined')
                DOMinput.setAttribute('placeholder', field.placeholder);
            if (typeof field.value !== 'undefined')
                DOMinput.value = field.value;
            if (typeof field.disabled === "boolean")
                DOMinput.disabled = field.disabled;
            if (typeof field.readonly === "boolean")
                DOMinput.readOnly = field.readonly;
            if (DOMinput.type === "number") {
                if (typeof field.min !== 'undefined')
                    DOMinput.setAttribute('min', field.min);
                if (typeof field.max !== 'undefined')
                    DOMinput.setAttribute('max', field.max);
                if (typeof field.step !== 'undefined')
                    DOMinput.setAttribute('step', field.step);
            }
            if (DOMinput.type === "file") {
                if (typeof field.accept !== 'undefined')
                    DOMinput.setAttribute('accept', field.accept);
            }
            else if (DOMinput.type === "checkbox") {
                if (field.value === true)
                    DOMinput.checked = true;
            }
            else if (DOMinput.type === "switch") {
                if (field.value === true)
                    DOMinput.checked = true;
            }
            else if (DOMinput.type === "hidden") {
                DOMtableTR.style.display = "none";
            }
            DOMmother.appendChild(DOMinput);
            if (field.type === "switch") {
                let DOMlabel = document.createElement('label');
                DOMlabel.setAttribute('for', field.name);
                DOMmother.appendChild(document.createTextNode('OFF'));
                DOMmother.appendChild(DOMlabel);
                DOMmother.appendChild(document.createTextNode('ON'));
            }
            if (typeof field.requires !== 'undefined')
                dialog.addRequires(DOMtableTR, field.requires);
            dialog.requires_ref.push(DOMinput);
            DOMinput.addEventListener('change', function (event) {
                dialog.require_check(event);
            }, false);
            if (typeof field.keyevents !== 'undefined' && field.keyevents === false) {
                DOMinput.addEventListener('keydown', function (event) {
                    if (['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'].includes(event.key)) {
                        event.preventDefault();
                        return false;
                    }
                });
            }
        },
        'select': function (dialog, DOMtableTR, field) {
            let DOM_tableTR_name = document.createElement('th');
            DOMtableTR.appendChild(DOM_tableTR_name);
            DOM_tableTR_name.className = 'r';
            DOM_tableTR_name.innerText = field.label;
            if (field.hasOwnProperty('tooltip') && typeof field.tooltip === 'string') {
                DOM_tableTR_name.innerHTML += '<div class="tooltip"><span class="material-icons clickable" style="font-size: 16px;margin-left: 8px;">help</span><div class="tooltip_text">' + field.tooltip + '</div></div>';
            }
            let DOM_tableTR_input = document.createElement('td');
            DOMtableTR.appendChild(DOM_tableTR_input);
            DOM_tableTR_input.className = 'c';
            let DOMselect = document.createElement('select');
            DOM_tableTR_input.appendChild(DOMselect);
            DOMselect.name = field.name;
            if (field.disabled === true) {
                DOMselect.setAttribute('disabled', 'disabled');
                DOMselect.name = field.name + '_disabled';
                let DOMhidden = document.createElement('input');
                DOM_tableTR_input.appendChild(DOMhidden);
                DOMhidden.type = 'hidden';
                DOMhidden.name = field.name;
                DOMhidden.value = field.value;
            }
            for (let i in field.options) {
                let opt = document.createElement('option');
                opt.appendChild(document.createTextNode(field.options[i]));
                opt.value = i;
                DOMselect.appendChild(opt);
            }
            DOMselect.value = field.value;
            if (typeof field.requires !== 'undefined')
                dialog.addRequires(DOMtableTR, field.requires);
            dialog.requires_ref.push(DOMselect);
            DOMselect.addEventListener('change', function (event) {
                dialog.require_check(event);
            }, false);
        },
        'richtext': function (dialog, DOMtableTR, field) {
            let DOM_tableTD = document.createElement('td');
            DOMtableTR.appendChild(DOM_tableTD);
            DOM_tableTD.className = 'l';
            DOM_tableTD.colSpan = 2;
            let DOM_tableTD_p = document.createElement('p');
            DOM_tableTD.appendChild(DOM_tableTD_p);
            DOM_tableTD_p.innerText = field.label;
            DOM_tableTD_p.style.textAlign = 'center';
            DOM_tableTD_p.className = 'table_cell_th';
            let DOMrichtext = document.createElement('div');
            DOM_tableTD.appendChild(DOMrichtext);
            DOMrichtext.className = 'richtext';
            let Toolbar1 = [
                ['Undo', 'undo', function () { formatDoc('undo'); }],
                ['Redo', 'redo', function () { formatDoc('redo'); }],
                ['Remove formatting', 'format_clear', function () { formatDoc('removeFormat'); }],
                ['Bold', 'format_bold', function () { formatDoc('bold'); }],
                ['Italic', 'format_italic', function () { formatDoc('italic'); }],
                ['Underline', 'format_underlined', function () { formatDoc('underline'); }],
                ['Left align', 'format_align_left', function () { formatDoc('justifyleft'); }],
                ['Center align', 'format_align_center', function () { formatDoc('justifycenter'); }],
                ['Right align', 'format_align_right', function () { formatDoc('justifyright'); }],
                ['Numbered list', 'format_list_numbered', function () { formatDoc('insertorderedlist'); }],
                ['Dotted list', 'format_list_bulleted', function () { formatDoc('insertunorderedlist'); }],
                ['Quote', 'format_quote', function () { formatDoc('formatblock', 'blockquote'); }],
                ['Add indentation', 'format_indent_decrease', function () { formatDoc('outdent'); }],
                ['Delete indentation', 'format_indent_increase', function () { formatDoc('indent'); }],
                ['Hyperlink', 'insert_link', function () {
                        const sLnk = prompt('Write the URL here', 'http:\/\/');
                        if (sLnk && sLnk !== '' && sLnk !== 'http://') {
                            formatDoc('createlink', sLnk);
                        }
                    }],
            ];
            let DOMtoolbar2 = document.createElement('div');
            DOMtoolbar2.className = 'richtext_toolbox2';
            DOMrichtext.appendChild(DOMtoolbar2);
            for (let button of Toolbar1) {
                let DOMtoolbar2button = document.createElement('button');
                DOMtoolbar2button.setAttribute('title', button[0]);
                DOMtoolbar2button.type = 'button';
                DOMtoolbar2button.innerHTML = '<span class="material-icons">' + button[1] + '</span>';
                DOMtoolbar2button.addEventListener('click', button[2]);
                DOMtoolbar2.appendChild(DOMtoolbar2button);
            }
            let DOMinput = document.createElement('input');
            DOMinput.type = 'hidden';
            DOMinput.name = field.name;
            DOMrichtext.appendChild(DOMinput);
            let DOMtextbox = document.createElement('div');
            DOMtextbox.className = "richtext_textbox";
            DOMtextbox.contentEditable = 'true';
            DOMtextbox.innerHTML = field.value;
            DOMrichtext.appendChild(DOMtextbox);
            dialog.submitEvents.push(function () {
                DOMinput.value = DOMtextbox.innerHTML;
            });
            function formatDoc(sCmd, sValue = '') {
                document.execCommand(sCmd, false, sValue);
                DOMtextbox.focus();
            }
        },
    };
    const dialog_render = {
        'html': function (dialog, definition) {
            let DOMdiv = document.createElement('div');
            DOMdiv.innerHTML = definition.value;
            return DOMdiv;
        },
        'form': function (dialog, definition) {
            let DOMtable = document.createElement('table');
            DOMtable.style.width = '100%';
            for (let i in definition.fields) {
                let field = definition.fields[i];
                let DOMtableTR = document.createElement('tr');
                DOMtable.appendChild(DOMtableTR);
                if (field.hasOwnProperty('selectable_id') && typeof field.selectable_id !== 'undefined') {
                    for (let selectable of field.selectable_id) {
                        let name = 'selectable_id_' + selectable[0] + '-' + selectable[1];
                        DOMtableTR.setAttribute(name, 'false');
                        if (!DOMtableTR.hasAttribute('selectable_ids'))
                            DOMtableTR.setAttribute('selectable_ids', name);
                        else
                            DOMtableTR.setAttribute('selectable_ids', DOMtableTR.getAttribute('selectable_ids') + " " + name);
                    }
                    DOMtableTR.style.display = 'none';
                }
                try {
                    dialog_field[field.type](dialog, DOMtableTR, field);
                }
                catch (e) {
                    dialog_field['input'](dialog, DOMtableTR, field);
                }
            }
            return DOMtable;
        },
        'button': function (dialog, definition) {
            let DOMdiv = document.createElement('div');
            DOMdiv.className = 'control';
            let value = definition.value;
            for (let i in value) {
                let but = value[i];
                if (but.type === "submit") {
                    let DOMbutton = document.createElement('input');
                    DOMbutton.type = but.type;
                    DOMbutton.value = but.label;
                    DOMdiv.appendChild(DOMbutton);
                    DOMbutton.addEventListener('click', function () {
                        dialog.DOMformSubmit.click();
                    });
                }
                else if (but.type === "cancel") {
                    let DOMbutton = document.createElement('button');
                    DOMbutton.type = 'button';
                    DOMbutton.className = 'form cancel';
                    DOMbutton.innerText = but.label;
                    DOMbutton.addEventListener('click', function () {
                        dialog.close();
                    });
                    DOMdiv.appendChild(DOMbutton);
                }
                else if (but.type === "reset") {
                    let DOMbutton = document.createElement('input');
                    DOMbutton.type = but.type;
                    DOMbutton.value = but.label;
                    DOMdiv.appendChild(DOMbutton);
                    DOMbutton.addEventListener('click', function () {
                    });
                }
            }
            return DOMdiv;
        }
    };
    class Dialog {
        constructor() {
            this.requires = [];
            this.requires_ref = [];
            this.callback = null;
            this.DOMdialog = document.createElement('div');
            this.DOMdialog.className = 'dialog';
            this.DOMdialog.setAttribute('visible', 'false');
            this.DOMdialog.addEventListener('click', function (e) {
                if ('className' in e.target && e.target.className === "dialog")
                    this.close();
            }.bind(this));
            this.DOMbox = document.createElement('div');
            this.DOMbox.className = 'box';
            this.DOMdialog.appendChild(this.DOMbox);
            this.DOMhead = document.createElement('h2');
            this.DOMhead.className = 'head';
            this.DOMbox.appendChild(this.DOMhead);
            this.DOMcontent = document.createElement('div');
            this.DOMcontent.className = 'content';
            this.DOMbox.appendChild(this.DOMcontent);
            document.body.appendChild(this.DOMdialog);
        }
        load(uri, data = null, callback = null) {
            this.callback = callback;
            dynamic_load(uri, function (response) {
                this.show(response.responseText);
            }.bind(this), data);
        }
        show(content) {
            let dialogJSON = JSON.parse(content);
            this.selection_id = [];
            this.finishEvents = [
                function () {
                    this.require_check();
                }.bind(this)
            ];
            this.submitEvents = [];
            this.DOMhead.innerText = dialogJSON.title;
            this.DOMcontent.innerHTML = '';
            let DOMform = document.createElement('form');
            if (typeof dialogJSON.action !== 'undefined') {
                let DOMerror = document.createElement('p');
                DOMerror.className = 'error-msg';
                DOMerror.innerText = '';
                this.DOMcontent.appendChild(DOMerror);
                DOMform.addEventListener('submit', function (event) {
                    event.preventDefault();
                    for (let i in this.submitEvents)
                        this.submitEvents[i]();
                    let formData = new FormData(DOMform);
                    formData.append('requested_uri', window.location);
                    if (dialogJSON.action.substr(0, 11) === 'javascript:') {
                        let func = window;
                        let parts = dialogJSON.action.substr(11).split('.');
                        for (let part of parts) {
                            func = func[part];
                        }
                        func.call(window[parts[0]], formData, this);
                    }
                    else
                        dynamic_load(dialogJSON.action, function (response) { this.result(response, DOMerror); }.bind(this), formData);
                    return false;
                }.bind(this));
                this.DOMcontent.appendChild(DOMform);
                this.DOMformSubmit = document.createElement('input');
                this.DOMformSubmit.type = 'submit';
                this.DOMformSubmit.style.display = 'none';
                this.DOMcontent.appendChild(this.DOMformSubmit);
            }
            for (let i in dialogJSON.content) {
                let content_item = dialogJSON.content[i];
                try {
                    let content = dialog_render[content_item.type](this, content_item);
                    if (typeof dialogJSON.action !== 'undefined')
                        DOMform.appendChild(content);
                    else
                        this.DOMcontent.appendChild(content);
                }
                catch (e) {
                    console.error(e);
                }
            }
            for (let i in this.finishEvents)
                this.finishEvents[i]();
            this.DOMdialog.setAttribute('visible', 'true');
        }
        close() {
            this.DOMdialog.setAttribute('visible', 'false');
        }
        result(response, DOMerror) {
            let result = JSON.parse(response.responseText);
            if (result.code === 1) {
                if (this.callback !== null) {
                    this.callback(result);
                    this.close();
                }
                else {
                    if (typeof result.redirect === 'string')
                        window.location = result.redirect;
                    else
                        location.reload();
                }
            }
            else {
                if (result.hasOwnProperty('message') && typeof result.message === 'string')
                    DOMerror.innerText = result.message;
                else
                    DOMerror.innerText = 'Something went wrong. Please try later again.';
            }
        }
        addRequires(element, requires) {
            this.requires.push([element, requires]);
        }
        require_check(event) {
            let map = [];
            for (let i in this.requires_ref) {
                map[this.requires_ref[i].name] = i;
            }
            for (let requireList of this.requires) {
                let require_check = true;
                for (let require of requireList[1]) {
                    if (this.requires_ref[map[require[0]]].type === "checkbox") {
                        if ((require[1] === "true") != (this.requires_ref[map[require[0]]].checked))
                            require_check = false;
                    }
                    else {
                        const regex = new RegExp(require[1]);
                        if (!regex.test(this.requires_ref[map[require[0]]].value))
                            require_check = false;
                    }
                }
                requireList[0].style = require_check ? "" : "display:none";
            }
        }
    }
    Maknapp.Dialog = Dialog;
    function addDialogField(name, method) {
        dialog_field[name] = method;
    }
    Maknapp.addDialogField = addDialogField;
    function dynamic_load(uri, element = null, data = null, method = "POST") {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    if (typeof element === "function")
                        element(this);
                    else if (element instanceof HTMLElement)
                        element.innerHTML = this.responseText;
                }
                else if (this.status === 404)
                    snackbar('Error at requesting dialog. Error code: ' + this.status + ' page not found');
                else if (this.status === 900)
                    snackbar('Error at requesting dialog. Error code: ' + this.status + ' page inactive');
                else
                    snackbar('Error at requesting dialog. Error code: ' + this.status);
            }
        };
        xhttp.withCredentials = true;
        xhttp.open(method, uri, true);
        xhttp.send(data);
    }
    Maknapp.dynamic_load = dynamic_load;
    function snackbar(content) {
        let element = document.createElement('DIV');
        element.className = 'snackbar';
        element.innerHTML = content;
        document.body.appendChild(element);
        element.style.marginBottom = "";
        setTimeout(function () { element.style.marginBottom = "0px"; }, 100);
        setTimeout(function () { element.style.marginBottom = ""; setTimeout(function () { document.body.removeChild(element); }, 400); }, 3000);
    }
    Maknapp.snackbar = snackbar;
})(Maknapp || (Maknapp = {}));
//# sourceMappingURL=Dialog.js.map