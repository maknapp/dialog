<?php

use Maknapp\Dialog\Dialog;

require_once('../vendor/autoload.php');
require_once('FieldCustom.php');

$dialog = new Dialog('dialog.example');
$dialog->title = "Edit user";
$dialog->action = "api.dialog.result.php";

$dialog->getElement('username')->disabled = true;

//$dialog->buttons = false;
$dialog->setHTML('<p>Hallo</p>');

header('content-type: application/json');
echo $dialog->getJSON(["username" => "f.maknapp"]);
