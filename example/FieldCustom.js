let a = function (dialog, DOMtableTR, field) {
    let th = document.createElement('th');
    th.innerText = field.label;
    th.className = 'r';
    DOMtableTR.appendChild(th);

    let td = document.createElement('td');
    DOMtableTR.appendChild(td);

    let box = document.createElement('div');
    box.style.background = "#FF00FF";
    box.style.height = "20px";
    box.style.width = "60px";

    td.appendChild(box);
}

Maknapp.addDialogField('custom', a);
