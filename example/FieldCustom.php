<?php

namespace Maknapp\Dialog;

class FieldCustom extends Field
{
    public function __construct(string $namespace, \SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);
    }

    public function getDialogField(array $values): array
    {
        $element = parent::getDialogField($values);
        $element['type'] = 'custom';

        return $element;
    }
}