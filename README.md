# Installation and Autoloading

``` json
  "repositories": [{
    "type": "composer",
    "url": "https://gitlab.com/api/v4/group/16729056/-/packages/composer/"
  }],
  "config": {
    "gitlab-domains": ["gitlab.com"]
  },
```

# Dependencies
This package requires PHP 8.0 or later.

# Documentation
...todo...